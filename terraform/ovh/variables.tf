variable "os_username" {}
variable "os_password" {}
variable "os_auth_url" {}
variable "os_tenant_name" {}
variable "os_tenant_id" {}

variable "ovh_endpoint" {
  default = "ovh-eu"
}

variable "ovh_region" {
    default = "GRA1"
}

variable "stack_name" {}
variable "os_net_id" {
}

variable "net_vlan_id" {
  default = 1
}

variable "cidr24_priv_block" {
  default = {
    GRA1 = "10.203.0.0/24"
    SBG1 = "10.203.1.0/24"
    BHS1 = "10.203.2.0/24"
  }
}


variable "bastion_flavor_id" {
  default = {
    GRA1 = "30fc1f25-348e-458f-bc0e-029a533c6c02"
    BHS1 = "eg-7-ssd"
    SBG1 = "eg-7-ssd"
  }
}

variable "ovh_public_network_name" {
  default = {
    GRA1 = "Ext-Net"
    BHS1 = "Ext-Net"
    SBG1 = "Ext-Net"
  }
}

variable "bastion_img_path"{
  default = "./coreos-blackfish-bastion.openstack"
}


variable "blackfish_img_path"{
  default = "./coreos-blackfish.openstack"
}

variable "swarm_volume_size"{
  default = "100"
}

variable "swarm_volume_type" {
  default = "classic"
}

variable "swarm_volumes_count" {
  default = "0"
}
