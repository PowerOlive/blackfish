resource "openstack_compute_keypair_v2" "bf_keypair" {
  name = "my-keypair"
  public_key = "${file("./${var.stack_name}.keypair.pub")}"
}
