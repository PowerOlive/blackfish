provider "ovh" {
  endpoint = "${var.ovh_endpoint}"
}

resource "ovh_publiccloud_user" "terraform" {
  project_id  = "${var.project_id}"
  description = "openstack blackfish user"
}

resource "ovh_vrack_publiccloud_attachment" "attach" {
  vrack_id   = "${var.vrack_id}"
  project_id = "${var.project_id}"
}

resource "ovh_publiccloud_private_network" "bf_net" {
  project_id  = "${ovh_vrack_publiccloud_attachment.attach.project_id}"
  vlan_id     = "${var.net_vlan_id}"
  name        = "${var.stack_name}_bf_net"
  regions     = ["${var.ovh_region}"]
}
