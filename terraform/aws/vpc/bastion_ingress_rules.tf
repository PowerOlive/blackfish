resource "aws_security_group_rule" "bastion_from_ssh" {
    type = "ingress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_group_id = "${aws_security_group.bastion.id}"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "bastion_from_vpn" {
    type = "ingress"
    from_port = 1194
    to_port = 1194
    protocol = "udp"
    security_group_id = "${aws_security_group.bastion.id}"
    cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "bastion_from_nodes_all_tcp_traffic" {
  type = "ingress"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  security_group_id = "${aws_security_group.bastion.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_from_nodes_all_udp_traffic" {
  type = "ingress"
  from_port = 0
  to_port = 65535
  protocol = "udp"
  security_group_id = "${aws_security_group.bastion.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
}
