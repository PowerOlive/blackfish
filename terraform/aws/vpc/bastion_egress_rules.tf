resource "aws_security_group_rule" "bastion_to_internet_tcp" {
  type = "egress"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  security_group_id = "${aws_security_group.bastion.id}"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "bastion_to_internet_udp" {
  type = "egress"
  from_port = 0
  to_port = 65535
  protocol = "udp"
  security_group_id = "${aws_security_group.bastion.id}"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "bastion_to_nodes_ssh" {
    type = "egress"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_nodes_control" {
  type = "egress"
  from_port = 4443
  to_port = 4443
  protocol = "tcp"
  security_group_id = "${aws_security_group.bastion.id}"
  source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_consul_cluster_tcp" {
    type = "egress"
    from_port = 8300
    to_port = 8302
    protocol = "tcp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_consul_cluster_udp" {
    type = "egress"
    from_port = 8300
    to_port = 8302
    protocol = "udp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_consul_agent" {
    type = "egress"
    from_port = 8500
    to_port = 8501
    protocol = "tcp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_consul_dns_tcp" {
    type = "egress"
    from_port = 8600
    to_port = 8600
    protocol = "tcp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_consul_dns_udp" {
    type = "egress"
    from_port = 8600
    to_port = 8600
    protocol = "udp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_dns_tcp" {
    type = "egress"
    from_port = 53
    to_port = 53
    protocol = "tcp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_dns_udp" {
    type = "egress"
    from_port = 53
    to_port = 53
    protocol = "udp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_swarm_manager" {
    type = "egress"
    from_port = 4000
    to_port = 4000
    protocol = "tcp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}

resource "aws_security_group_rule" "bastion_to_docker_registry" {
    type = "egress"
    from_port = 5000
    to_port = 5000
    protocol = "tcp"
    security_group_id = "${aws_security_group.bastion.id}"
    source_security_group_id = "${aws_security_group.nodes.id}"
}
