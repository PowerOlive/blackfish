#!/bin/bash -e
source "$(dirname "$0")/functions.sh"

# Usage info
show_help() {
    cat << EOF
Usage: ${0##*/} [-hv] start|stop|rm
Handles the lifecycle of a haproxy-consul service

COMMANDS:
    start              Starts haproxy
    stop               Stops haproxt
    rm                 rm rkt container

OPTIONS:
    -h                 display this help and exit
    -v                 verbose mode. Can be used multiple
                       times for increased verbosity.
EOF
}

start() {
    #Generate an upstream resolv.conf for dnsmasq
    RESOLV_CONF=$(mktemp)
    HOSTIP=$(gethostadminipaddr)
    DNS=$(awk -F = '/^DNS=/ {print $2}' /var/run/systemd/netif/state | paste -sd ,)

    (IFS=,; for i in $DNS; do echo "nameserver $i"; done) > $RESOLV_CONF

    /usr/bin/rkt run \
                 --net=host \
                 --hostname="$(hostname)" \
                 --volume resolv,kind=host,source=$RESOLV_CONF,readOnly=true \
                 --mount volume=resolv,target=/etc/resolv.dnsmasq.conf \
                 --uuid-file-save "$UUID_FILE" \
                 $(aci_id "andyshinn/dnsmasq") --exec /usr/sbin/dnsmasq -- \
                 -d \
                 --host-record=metadata,${HOSTIP} \
                 --host-record=consul,${HOSTIP} \
                 --resolv-file=/etc/resolv.dnsmasq.conf \
                 -S "/$STACK_NAME/$HOSTIP#8600"
}

shift "$((OPTIND-1))" # Shift off the options and optional --.

case $1 in
    start)
        start
        ;;
    rm)
        rm_rkt
        ;;
    stop)
        stop_rkt
        ;;
    *)
        log user.error "unknown command: $1"
        exit 1
        ;;
esac
