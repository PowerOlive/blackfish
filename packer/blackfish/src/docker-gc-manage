#!/bin/bash -e
source "$(dirname "$0")/functions.sh"

# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-hv] start|stop|rm
Handles the lifecycle of a docker-gc service

COMMANDS:
    start              Starts haproxy
    stop               Stops haproxt
    rm                 rm rkt container

OPTIONS:
    -h                 display this help and exit
    -v                 verbose mode. Can be used multiple
                       times for increased verbosity.
EOF
}

start(){
    log user.info "start"
    rkt run \
        --set-env=GRACE_PERIOD_SECONDS=604800 \
        --set-env=FORCE_IMAGE_REMOVAL=1 \
        --volume docker,kind=host,source=/var/run/docker.sock,readOnly=false \
        --mount volume=docker,target=/var/run/docker.sock \
        --uuid-file-save "$UUID_FILE" \
        $(aci_id "spotify/docker-gc")
}

OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts ":hv:n:" opt; do
    case "$opt" in
        h)
            show_help
            exit 0
            ;;
        v)  verbose=$((verbose+1))
            ;;
        '?')
            show_help >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))" # Shift off the options and optional --.

case $1 in
    start)
        start
        ;;
    rm)
        rm_rkt
        ;;
    stop)
        stop_rkt
        ;;
    *)
        log user.error "unknown command: $1"
        exit 1
        ;;
esac
